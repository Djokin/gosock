package gosock

import (
	"io"
)

func Forward(dst *Session, src *Session) error {
	var packet = make([]byte, 2048)
	for {
		num, err := src.Recv(packet)
		if nil != err {
			if err != io.EOF {
				return err
			} else {
				return nil
			}
		}
		if num > 0 {
			if _, err = dst.Send(packet[0:num]); nil != err {
				return err
			}
		}
	}

	return nil
}
