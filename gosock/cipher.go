package gosock

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"errors"
)

const (
	md5len = 16

	aeskey128 = 16
	aeskey192 = 24
	aeskey256 = 32
)

//Cipher , encrypt/decrypt
type Cipher struct {
	enc cipher.Stream
	dec cipher.Stream
}

func (cip *Cipher) Encrypt(dst, src []byte) {
	if len(dst) < len(src) {
		return
	}
	cip.enc.XORKeyStream(dst, src)
}

func (cip *Cipher) Decrypt(dst, src []byte) {
	cip.dec.XORKeyStream(dst, src)
}

//calc md5 and change array to slice
func md5sum(ori []byte) []byte {
	m := md5.Sum(ori)
	return m[:]
}

func genKeyIV(keylen int, keytext string) ([]byte, []byte) {
	var keyb = make([]byte, 32, 32)

	m := md5sum([]byte(keytext))
	copy(keyb, m)
	copy(keyb[md5len:], md5sum(m[:md5len/2]))
	return keyb[:keylen], md5sum(m[md5len/2:])
}

func newAesCFBcipher(keylen int, keytext string) (*Cipher, error) {
	var cip = new(Cipher)
	key, iv := genKeyIV(keylen, keytext)

	block, err := aes.NewCipher(key)
	if nil != err {
		return nil, err
	}

	cip.enc = cipher.NewCFBEncrypter(block, iv)
	cip.dec = cipher.NewCFBDecrypter(block, iv)

	return cip, nil
}

func newAesCFBcipher128(keytext string) (*Cipher, error) {
	return newAesCFBcipher(aeskey128, keytext)
}

func newAesCFBcipher192(keytext string) (*Cipher, error) {
	return newAesCFBcipher(aeskey192, keytext)
}

func newAesCFBcipher256(keytext string) (*Cipher, error) {
	return newAesCFBcipher(aeskey256, keytext)
}

type cipherfunc func(string) (*Cipher, error)

var ciphermap = map[string]cipherfunc{
	"aes-128-cfb": newAesCFBcipher128,
	"aes-192-cfb": newAesCFBcipher192,
	"aes-256-cfb": newAesCFBcipher256,
}

//NewCipher , generate New ciphers
func NewCipher(ciphertype string, keytext string) (*Cipher, error) {
	if cifunc, ok := ciphermap[ciphertype]; ok {
		return cifunc(keytext)
	} else {
		return nil, errors.New("Not support encrypt func " + ciphertype)
	}
}
