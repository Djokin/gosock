package gosock

import "testing"

func TestCipher192(t *testing.T) {
	b := []byte("www.baidu.com")
	var e = make([]byte, 128)
	var d = make([]byte, 128)

	t.Log("Orign text : ", b)

	ci, _ := NewCipher("aes-192-cfb", "123456")
	ci.Encrypt(e, b)

	t.Log("Get encrypt : ", e)

	ci.Decrypt(d, e[:len(b)])

	t.Log("Get decrypt : ", d)
}

func TestCipher128(t *testing.T) {
	b := []byte("www.baidu.com")
	var e = make([]byte, 128)
	var d = make([]byte, 128)

	t.Log("Orign text : ", b)

	ci, _ := NewCipher("aes-128-cfb", "123456")
	ci.Encrypt(e, b)

	t.Log("Get encrypt : ", e)

	ci.Decrypt(d, e[:len(b)])

	t.Log("Get decrypt : ", d)
}

func TestCipher256(t *testing.T) {
	b := []byte("www.baidu.com")
	var e = make([]byte, 128)
	var d = make([]byte, 128)

	t.Log("Orign text : ", b)

	ci, _ := NewCipher("aes-256-cfb", "123456")
	ci.Encrypt(e, b)

	t.Log("Get encrypt : ", e)

	ci.Decrypt(d, e[:len(b)])

	t.Log("Get decrypt : ", d)
}
