package gosock

import (
	"encoding/json"
	"os"
)

type Config struct {
	Server []struct {
		Ipaddr  string `json:"ipaddr"`
		Crypto  string `json:"crypto"`
		Key     string `json:"key"`
		NoAvail bool   `json:"noavail"`
	} `json:"server"`
	Local string `json:"local"`
}

func ParseConf(confile string, conf *Config) error {
	file, err := os.OpenFile(confile, os.O_RDONLY, 0666)
	if nil != err {
		return err
	}
	defer file.Close()

	info, err := file.Stat()
	if nil != err {
		return err
	}

	var confbyte = make([]byte, info.Size())
	if _, err = file.Read(confbyte); nil != err {
		return err
	}

	return json.Unmarshal(confbyte, conf)
}
