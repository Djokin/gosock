package gosock

import (
	"net"
)

//Session struct based on tcp proto
type Session struct {
	cip  *Cipher
	conn net.Conn
}

//InitCrypto , init crypto info
func (c *Session) InitCrypto(ciphertype string, key string) error {
	var err error
	c.cip, err = NewCipher(ciphertype, key)

	return err
}

//Recv , recv messages ...
func (c *Session) Recv(b []byte) (int, error) {
	num, err := c.conn.Read(b)

	if num > 0 && nil == err && nil != c.cip {
		c.cip.Decrypt(b, b[0:num])
	}

	return num, err
}

//Send , send messages ...
func (c *Session) Send(b []byte) (int, error) {
	if nil != c.cip {
		c.cip.Encrypt(b, b)
	}

	return c.conn.Write(b)
}

//Close , close session
func (c *Session) Close() {
	c.conn.Close()
}

//NewSession creates new Session object
func NewSession(conn net.Conn) *Session {
	return &Session{
		conn: conn}
}
