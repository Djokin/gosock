package gosock

import "fmt"

const (
	lerror  = 1
	lwarn   = 2
	lnotice = 3
	ldebug  = 4
)

func log(level int, a ...interface{}) {
	fmt.Println(a...)
}

//Error , log error messages
func Error(a ...interface{}) {
	log(lerror, a...)
}

//Warning , log error messages
func Warning(a ...interface{}) {
	log(lwarn, a...)
}

//Notice , log error messages
func Notice(a ...interface{}) {
	log(lnotice, a...)
}

//Debug , log error messages
func Debug(a ...interface{}) {
	log(ldebug, a...)
}
