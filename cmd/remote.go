package main

import (
	"flag"
	gs "gosock/gosock"
	"log"
	"net"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
)

const (
	inxAtyp = 0
	inxAddr = 1
)

var gconf gs.Config

func remoteShake(sess *gs.Session) (string, error) {
	var packet = make([]byte, 512)
	var num = 0

	for {
		var err error
		if num, err = sess.Recv(packet); nil != err {
			return "", err
		}

		if num > inxAddr && num >= int(packet[inxAddr]+4) {
			break
		}
	}

	addr := string(packet[inxAddr+1 : inxAddr+packet[inxAddr]+1])
	port := strconv.Itoa(int(packet[inxAddr+packet[inxAddr]+1])<<8 + int(packet[inxAddr+packet[inxAddr]+2]))

	sess.Send([]byte{0x0, 0x0, 0x0, 0x0})

	gs.Debug("Socked for " + addr + ":" + port)

	return addr + ":" + port, nil
}

func remoteHandle(sess *gs.Session) {
	defer sess.Close()

	ipaddr, err := remoteShake(sess)
	if nil != err {
		gs.Error(err)
		return
	}

	rconn, err := net.Dial("tcp", ipaddr)
	if nil != err {
		gs.Error(err)
		return
	}

	rsess := gs.NewSession(rconn)
	defer rsess.Close()

	go gs.Forward(sess, rsess)
	gs.Forward(rsess, sess)
}

func mainloop() {
	var wg sync.WaitGroup

	for _, server := range gconf.Server {
		if len(server.Ipaddr) > 0 && !server.NoAvail {
			var ipaddr string
			if strings.Contains(server.Ipaddr, ":") {
				ipaddr = server.Ipaddr
			} else {
				//It only has port
				ipaddr = "0.0.0.0:" + server.Ipaddr
			}

			wg.Add(1)
			go func() {
				defer wg.Add(-1)

				myserver := server

				l, err := net.Listen("tcp", ipaddr)
				if nil != err {
					gs.Error(err)
					return
				}
				defer l.Close()

				for {
					conn, err := l.Accept()
					if nil != err {
						gs.Error(err)
						return
					}

					sess := gs.NewSession(conn)

					if len(myserver.Crypto) > 0 && len(myserver.Key) > 0 {
						sess.InitCrypto(myserver.Crypto, myserver.Key)
					}
					go remoteHandle(sess)
				}
			}()
		}
	}

	wg.Wait()
}

func initSig() {
	c := make(chan os.Signal)
	signal.Notify(c, syscall.SIGINT, syscall.SIGSEGV, syscall.SIGQUIT)

	go func() {
		for s := range c {
			switch s {
			case syscall.SIGINT, syscall.SIGSEGV, syscall.SIGQUIT:
				os.Exit(0)
			}
		}
	}()
}

func main() {
	gs.Notice("Remote start ... ")

	go func() {
		log.Println(http.ListenAndServe("0.0.0.0:8080", nil))
	}()

	confile := flag.String("-f", "./gosock_remote.json", "config file")
	flag.Parse()

	if err := gs.ParseConf(*confile, &gconf); nil != err {
		gs.Error(err)
		return
	}

	initSig()

	mainloop()
}
