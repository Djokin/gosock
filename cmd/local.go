package main

import (
	"errors"
	"flag"
	gs "gosock/gosock"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"
)

const (
	//sock version
	sock5 = 0x05

	//sock connect packet index
	inxVer  = 0
	inxCmd  = 1
	inxAtyp = 3
	inxAddr = 4

	//addr type
	typIpv4 = 1
	typDom  = 3
	typIpv6 = 4
)

var gconf gs.Config

//return the connect type + len + address + port
func sock5Shake(sess *gs.Session) ([]byte, error) {
	var packet = make([]byte, 512)
	_, err := sess.Recv(packet)
	if nil != err {
		return nil, err
	}

	if packet[inxVer] != sock5 {
		return nil, errors.New("Not sock5 ... ")
	}

	//no auth
	_, err = sess.Send([]byte{sock5, 0})
	if nil != err {
		return nil, err
	}

	_, err = sess.Recv(packet)
	if nil != err {
		return nil, err
	}

	if packet[inxAtyp] != typIpv4 && packet[inxAtyp] != typDom && packet[inxAtyp] != typIpv6 {
		gs.Debug(packet, packet[inxAtyp])
		return nil, errors.New("Unknown addr type")
	}

	sess.Send([]byte{sock5, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0})

	return packet[inxAtyp : inxAddr+1+packet[inxAddr]+2], nil
}

func connectToRemote() (*gs.Session, error) {
	serverNum := len(gconf.Server)
	for inx := 0; inx < serverNum; inx++ {
		server := gconf.Server[inx]

		if false == server.NoAvail {
			conn, err := net.Dial("tcp", server.Ipaddr)
			if nil != err {
				gs.Warning(err)
				continue
			}

			sess := gs.NewSession(conn)
			if len(server.Crypto) > 0 && len(server.Key) > 0 {
				sess.InitCrypto(server.Crypto, server.Key)
			}

			return sess, nil
		}
	}

	return nil, errors.New("Can't connect to any server ... ")
}

func remoteShake(rsess *gs.Session, addr []byte) error {
	packet := make([]byte, 256)
	if _, err := rsess.Send(addr); nil != err {
		return err
	}

	if _, err := rsess.Recv(packet); nil != err {
		return err
	}

	return nil
}

func localHandle(sess *gs.Session) {
	defer sess.Close()
	addr, err := sock5Shake(sess)
	if nil != err {
		gs.Error(err)
		return
	}

	rsess, err := connectToRemote()
	if nil != err {
		gs.Error(err)
		return
	}
	defer rsess.Close()

	if err = remoteShake(rsess, addr); nil != err {
		gs.Error(err)
		return
	}

	go gs.Forward(sess, rsess)
	gs.Forward(rsess, sess)
}

func getLocalConfAddr() string {
	if len(gconf.Local) == 0 {
		return "127.0.0.1:1080"
	}

	if strings.Contains(gconf.Local, ":") {
		return gconf.Local
	} else {
		return "127.0.0.1:" + gconf.Local
	}
}

func mainloop() {
	l, err := net.Listen("tcp", getLocalConfAddr())
	if nil != err {
		gs.Error(err)
		return
	}
	defer l.Close()

	for {
		conn, err := l.Accept()
		if nil != err {
			gs.Error(err)
			return
		}

		sess := gs.NewSession(conn)
		go localHandle(sess)
	}
}

func initSig() {
	c := make(chan os.Signal)
	signal.Notify(c, syscall.SIGINT, syscall.SIGSEGV, syscall.SIGQUIT)

	go func() {
		for s := range c {
			switch s {
			case syscall.SIGINT, syscall.SIGSEGV, syscall.SIGQUIT:
				os.Exit(0)
			}
		}
	}()
}

func main() {
	gs.Notice("Local start ... ")

	confile := flag.String("f", "./gosock_local.json", "Conf file")
	flag.Parse()

	if err := gs.ParseConf(*confile, &gconf); nil != err {
		gs.Error(err)
		return
	}

	initSig()

	mainloop()
}
